package com.alimuzaffar.mindreader;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.FaceDetector;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;


public class CameraPreviewCallback implements PreviewCallback {
	
	private static final String TAG = CameraPreviewCallback.class.getSimpleName();

	private long frames = 0;
	Point screenSize = null;
	
	private Context mContext;
	private boolean mFrontFacing = false;
	private int mCameraIndex = 0;
	private int rotation = 0;
	
	public CameraPreviewCallback(Context context, int cameraIndex) {
		mContext = context;
		mCameraIndex = cameraIndex;
		
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Camera.getCameraInfo(mCameraIndex, cameraInfo);
		mFrontFacing = (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
	}
	
	@Override
	public void onPreviewFrame(final byte[] data, final Camera camera) {

		if (frames % 16 != 0) {
			frames++;
			return;
		} else {
			frames++;
		}
		final int NUM_FACES = 1;
		if (data != null) {
			new Thread(new Runnable(){

				@Override
				public void run() {
					
					int width = 0;
					int height = 0;
					try {
						Size size = camera.getParameters().getPreviewSize();
						width = size.width;
						height = size.height;
					} catch (RuntimeException re) {
						//most likely camera was released.
						return;
					}

					final int[] rgb = decodeYUV420SP(data, width, height);
								
					Bitmap bmp = Bitmap.createBitmap(rgb, width, height, Bitmap.Config.RGB_565);
					// recreate the new Bitmap
					if(rotation != 0) {
						// createa matrix for the manipulation
				        Matrix matrix = new Matrix();
				        // rotate the Bitmap
				        matrix.postRotate(rotation);
						Bitmap rotatedBitmap = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);
						bmp.recycle();
						bmp = rotatedBitmap;
					}

					ArrayList<float[]> vals = findFacePosition(bmp, NUM_FACES, getScreenSize());
					if (mContext != null && !((MainActivity) mContext).isFinishing()) {
						((MainActivity) mContext).drawQuote(vals.get(0), vals.get(1));
					}
				}}).start();
		}

		
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(13)
	public Point getScreenSize() {
		if (screenSize == null) {
			WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			screenSize = new Point();

			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
				screenSize.x = display.getWidth();
				screenSize.y = display.getHeight();
			} else {
				display.getSize(screenSize);
			}
		}
		return screenSize;
	}
	
	public int[] decodeYUV420SP(byte[] yuv420sp, int width, int height) {

		final int frameSize = width * height;

		int rgb[] = new int[width * height];
		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0)
					y = 0;
				/*
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}

				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);

				if (r < 0)
					r = 0;
				else if (r > 262143)
					r = 262143;
				if (g < 0)
					g = 0;
				else if (g > 262143)
					g = 262143;
				if (b < 0)
					b = 0;
				else if (b > 262143)
					b = 262143;

				rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
				*/
				rgb[yp] = y;

			}
		}
		return rgb;
	}
	
	public ArrayList<float[]> findFacePosition(Bitmap bmp, int numFaces, Point ss) {
		FaceDetector.Face faces[] = new FaceDetector.Face[numFaces];
		if(ss == null)
			ss = new Point(bmp.getWidth(), bmp.getHeight());
		FaceDetector arrayFaces = new FaceDetector(bmp.getWidth(), bmp.getHeight(), numFaces);
		int found = arrayFaces.findFaces(bmp, faces);
		bmp.recycle();

		PointF midpoint = new PointF();
		float[] fpx = null;
		float[] fpy = null;
		if (found > 0) {
			fpx = new float[found];
			fpy = new float[found];
		}

		//Log.d("FACE FINDER", "FOUND " + found);
		for (int i = 0; i < found; i++) {
			//Log.d("FACE FINDER", "CONFIDENCE " + faces[i]);

			try {
				faces[i].getMidPoint(midpoint);

				//Log.d(TAG, "SX="+ss.x+", SY="+ss.y);
				float pcx = ((midpoint.x / (float) bmp.getWidth()));
				float pcy = ((midpoint.y / (float) bmp.getHeight()));
				
				if (mFrontFacing)
					fpx[i] = ss.x - (pcx * ss.x);
				else
					fpx[i] = (pcx * ss.x);
				fpy[i] = pcy * ss.y;
				//Log.d(TAG, "X=" + midpoint.x + ", Y=" + midpoint.y);
				//Log.d(TAG, "PX=" + fpx[i] + ", PY=" + fpy[i]);

			} catch (Exception e) {
				Log.e(TAG, "setFace(): face " + i + ": " + e.toString());
			}
		}

		ArrayList<float[]> vals = new ArrayList<float[]>();
		vals.add(fpx);
		vals.add(fpy);
		return vals;
	}
	
	public void setRotation(int degrees) {
		rotation = degrees;
	}

}
