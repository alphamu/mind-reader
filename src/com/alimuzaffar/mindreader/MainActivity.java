package com.alimuzaffar.mindreader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.update.JSONArray;
import org.json.update.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.alimuzaffar.mindreader.AppSettings.Key;

@SuppressLint("NewApi")
public class MainActivity extends Activity {
	public static final String TAG = MainActivity.class.getSimpleName();

	public byte[] cameraImg;
	public byte[] drawImg;

	PreviewSurfaceView mPreviewSurface;
	DrawSurfaceView mDrawSurface;
	CameraPreviewCallback cameraPreviewCallback;
	Camera mCamera;
	int numberOfCameras;
	int cameraCurrentlyLocked;
	int rotation = 0;
	Handler handler = new Handler();

	// The first rear facing camera
	int defaultCameraId;

	public static int RES_ID = 20120902;

	private ArrayList<String> thoughts = null;

	@SuppressLint("SimpleDateFormat")
	private static final DateFormat FILE_NAME_FORMAT = new SimpleDateFormat("yyyyMMdd_HHMMss");

	private int quoteIndex = -1;
	private static final String APP_FOLDER = Environment.getExternalStorageDirectory().toString() + "/Pictures/Mind Reader";
	private static final String APP_FOLDER_TMP = APP_FOLDER + "/tmp";
	File picture = new File(APP_FOLDER_TMP + "/picture-mind-reader-tmp-00.jpg");
	File speech = new File(APP_FOLDER_TMP + "/speech-mind-reader-tmp-00.png");
	File combined = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "ON CREATE");
		File folder = new File(APP_FOLDER_TMP);
		if (!folder.exists()) {
			folder.mkdirs();
		}

		// Hide the window title.
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		View rootView = getWindow().getDecorView();

		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB_MR2)
			rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);

		// Create a RelativeLayout container that will hold a SurfaceView,
		// and set it as the content of our activity.
		setContentView(R.layout.activity_main);

		mPreviewSurface = (PreviewSurfaceView) findViewById(R.id.cameraSurfaceView1);
		mDrawSurface = (DrawSurfaceView) findViewById(R.id.drawSurfaceView);

		updateThoughts();

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		AppRater.app_launched(this);

		ChangeLog cl = new ChangeLog(this);
		if (cl.firstRun())
			cl.getLogDialog().show();

		ImageView watermark = (ImageView) findViewById(R.id.watermark);
		watermark.setVisibility(View.INVISIBLE);

		// Find the total number of cameras available
		numberOfCameras = Camera.getNumberOfCameras();
		
		if(numberOfCameras == 0) {
			Toast.makeText(this, "Thought reader app will only work on devices with at least 1 camera.",Toast.LENGTH_LONG).show();
			finish();
		}

		// Find the ID of the default camera
		CameraInfo cameraInfo = new CameraInfo();
		for (int i = 0; i < numberOfCameras; i++) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
				defaultCameraId = i;
			}
		}
		
		detectCameras();
		
		if(getIntent() != null && getIntent().getBooleanExtra("splash", false)) {
			getIntent().putExtra("splash", false);
			final View splash = findViewById(R.id.splash);
			splash.setVisibility(View.VISIBLE);
			handler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					splash.setVisibility(View.GONE);					
				}
			}, 3000);
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "ON START");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "ON RESUME");
		setupActivity();
	}
	
	public void setupActivity() {
		AppSettings settings = AppSettings.getInstance(this);
		int temp = settings.getInt(Key.CAMERA_NUMBER, -1);
		if (temp != -1) {
			defaultCameraId = temp;
		}
		// Open the default i.e. the first rear facing camera.
		mCamera = Camera.open(defaultCameraId);
		setCameraDisplayOrientation(defaultCameraId, mCamera);
		cameraCurrentlyLocked = defaultCameraId;
		mPreviewSurface.setCamera(mCamera);
		mCamera.setPreviewCallback(cameraPreviewCallback = new CameraPreviewCallback(this, cameraCurrentlyLocked));
		cameraPreviewCallback.setRotation(rotation);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "ON PAUSE");
		stopActivity();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "ON STOP");
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "ON DESTROY");
	}
	
	public void stopActivity() {
		// Because the Camera object is a shared resource, it's very
		// important to release it when the activity is paused.
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mPreviewSurface.setCamera(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private void detectCameras() {
		if(numberOfCameras < 2) {
			findViewById(R.id.change_camera).setVisibility(View.GONE);
		}
	}

	public void switchCamera(View v) {
		// check for availability of multiple cameras
		if (numberOfCameras == 1) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(this.getString(R.string.camera_alert)).setNeutralButton("Close", null);
			AlertDialog alert = builder.create();
			alert.show();
			return;
		}

		// OK, we have multiple cameras.
		// Release this camera -> cameraCurrentlyLocked
		if (mCamera != null) {
			mCamera.stopPreview();
			mPreviewSurface.setCamera(null);
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}

		// Acquire the next camera and request Preview to reconfigure
		// parameters.
		mCamera = Camera.open((cameraCurrentlyLocked + 1) % numberOfCameras);
		cameraCurrentlyLocked = (cameraCurrentlyLocked + 1) % numberOfCameras;
		//		mCameraPreview.switchCamera(mCamera);
		//		mCamera.setPreviewCallback(new CameraPreviewCallback(this, cameraCurrentlyLocked));
		//
		//		// Start the preview
		//		mCamera.startPreview();
		AppSettings settings = AppSettings.getInstance(this);
		settings.set(Key.CAMERA_NUMBER, cameraCurrentlyLocked);
		Intent intent = getIntent();
		finish();
		startActivity(intent);

	}

	@Override
	public void onBackPressed() {
		Intent data = new Intent();
		data.putExtra("action", "close");
		if (getParent() == null) {
			setResult(Activity.RESULT_OK, data);
		} else {
			getParent().setResult(Activity.RESULT_OK, data);
		}

		super.onBackPressed();
		finish();
	}

	public void btn_screenshot(final View v) {
		playSnapSound();

		// image naming and path  to include sd card  appending name you choose for file
		ViewGroup buttons = (ViewGroup) findViewById(R.id.buttons_layout);
		buttons.setVisibility(View.GONE);
		ViewGroup snap = (ViewGroup) findViewById(R.id.snap_layout);
		ImageView watermark = (ImageView) findViewById(R.id.watermark);
		snap.setVisibility(View.GONE);
		watermark.setVisibility(View.VISIBLE);

		// create bitmap screen capture
		Bitmap bitmap;
		View v1 = getWindow().getDecorView();
		v1.setBackgroundColor(Color.TRANSPARENT);

		v1.setDrawingCacheEnabled(true);
		//bitmap = Bitmap.createBitmap(v1.getDrawingCache(true));
		Size size = mPreviewSurface.getPictureSize();
		if(rotation != 0) {
			bitmap = Bitmap.createScaledBitmap(v1.getDrawingCache(true), size.height, size.width, false);
		} else {
			bitmap = Bitmap.createScaledBitmap(v1.getDrawingCache(true), size.width, size.height, false);
		}
		v1.setDrawingCacheEnabled(false);

		OutputStream fout = null;

		try {
			fout = new FileOutputStream(speech);
			bitmap.compress(Bitmap.CompressFormat.PNG, 90, fout);
			fout.flush();
			fout.close();
			getCameraImage(v, bitmap);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			bitmap.recycle();
			buttons.setVisibility(View.VISIBLE);
			snap.setVisibility(View.VISIBLE);
			watermark.setVisibility(View.INVISIBLE);
		}
	}

	public void getCameraImage(View v, final Bitmap bitmapOne) {

		// create bitmap screen capture
		mPreviewSurface.setScreenshot(new Camera.PictureCallback() {

			@Override
			public void onPictureTaken(byte[] arg0, Camera arg1) {
				// TODO Auto-generated method stub
				Log.d(TAG, "RAW PICTURE");
			}
		},

		new Camera.PictureCallback() {

			@Override
			public void onPictureTaken(final byte[] img, final Camera cam) {
				// TODO Auto-generated method stub
				Log.d("Test", "JPEG PICTURE");
				cameraImg = img;
				new Thread(new Runnable(){

					@Override
					public void run() {
						Bitmap bitmapTwo = BitmapFactory.decodeByteArray(img, 0, img.length);
						writeToFile(picture, bitmapTwo, true);
						bitmapTwo.recycle();
						overlay(cam);
						
					}}).start();
				mPreviewSurface.getCamera().startPreview();
			}
		});

	}

	/*
	private byte[] bmpToByteArray(Bitmap bmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}
	*/

	public void writeToFile(File file, byte[] array) {
		try {
			FileOutputStream stream = new FileOutputStream(file);
			stream.write(array);
			stream.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG, e.getMessage(), e);
		} catch (IOException ioe) {
			Log.e(TAG, ioe.getMessage(), ioe);
		}
	}

	public void writeToFile(File file, Bitmap bitmap, boolean rotate) {
		OutputStream fout = null;

		try {

			if (rotate && rotation != 0) {
				// createa matrix for the manipulation
		        Matrix matrix = new Matrix();
		        // rotate the Bitmap
		        matrix.postRotate(rotation);
				Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
				bitmap.recycle();
				bitmap = rotatedBitmap;
			}

			fout = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
			fout.flush();
			fout.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.getMessage(), e);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
		} finally {
			bitmap.recycle();
		}
	}

	private void playSnapSound() {
		try {
			MediaPlayer mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.camera_shutter_click_08);

			final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

			if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.setLooping(false);
				mediaPlayer.start();
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settings, menu);

		if (v.getId() == R.id.menu_settings) {
			menu.setHeaderTitle(R.string.menu_settings);

			menu.findItem(R.id.txt_settings).setVisible(true);
			menu.setGroupVisible(R.id.resolution, false);

		} else if (v.getId() == R.id.menu_resolution) {
			CameraInfo cameraInfo = new CameraInfo();
			Camera.getCameraInfo(cameraCurrentlyLocked, cameraInfo);

			menu.setHeaderTitle(R.string.menu_resolution);

			menu.findItem(R.id.txt_settings).setVisible(false);
			menu.setGroupVisible(R.id.resolution, true);
			int selectedRes = 0;
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
				selectedRes = AppSettings.getInstance(getApplicationContext()).getInt(Key.FRONT_SELECTED_RESOLUTION, -1);
			else
				selectedRes = AppSettings.getInstance(getApplicationContext()).getInt(Key.SELECTED_RESOLUTION, -1);

			MenuItem def = null;
			boolean selectedFound = false;
			Camera.Parameters params = mPreviewSurface.getCamera().getParameters();
			List<Camera.Size> sizes = params.getSupportedPictureSizes();
			for (int i = 0; i < sizes.size(); i++) {
				Camera.Size size = sizes.get(i);
				MenuItem item = menu.add(R.id.resolution, RES_ID + i, Menu.NONE, size.width + "x" + size.height);
				if (size.width == 640 && size.height == 480) {
					def = item;
				}
				if ((RES_ID + i == selectedRes)) {
					item.setChecked(true);
					selectedFound = true;
				} else {
					item.setChecked(false);
				}
			}
			if (!selectedFound) {
				if (def == null)
					def = menu.getItem(sizes.size() / 2);

				def.setChecked(true);
			}
			menu.setGroupCheckable(R.id.resolution, true, true);

		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.txt_settings:
			showTextSettings();
			break;
		default:
			if (item.getGroupId() == R.id.resolution) {
				CameraInfo cameraInfo = new CameraInfo();
				Camera.getCameraInfo(cameraCurrentlyLocked, cameraInfo);

				item.setChecked(true);

				AppSettings settings = AppSettings.getInstance(getApplicationContext());
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
					settings.set(Key.FRONT_SELECTED_RESOLUTION, item.getItemId());
				else
					settings.set(Key.SELECTED_RESOLUTION, item.getItemId());

				setCameraResolution();
			}
			return super.onOptionsItemSelected(item);
		}

		return super.onOptionsItemSelected(item);
	}

	private void setCameraResolution() {
		int res = AppSettings.getInstance(getApplicationContext()).getInt(Key.SELECTED_RESOLUTION) % RES_ID;

		mPreviewSurface.getCamera().stopPreview();
		Camera.Parameters parameters = mPreviewSurface.getCamera().getParameters();
		List<Camera.Size> csc = parameters.getSupportedPictureSizes();
		Camera.Size cs = csc.get(res);
		Log.d("HERE", "Index=" + res + ", W=" + cs.width + ", H=" + cs.height);
		parameters.setPictureSize(cs.width, cs.height);
		mPreviewSurface.getCamera().setParameters(parameters);
		mPreviewSurface.getCamera().startPreview();
		mPreviewSurface.setPictureSize(cs);
	}

	public void showSettings(View v) {
		MainActivity.this.registerForContextMenu(v);
		MainActivity.this.openContextMenu(v);
	}

	private void showTextSettings() {
		Intent intent = new Intent(this, SpeechBubbleTextActivity.class);
		startActivityForResult(intent, 12);
	}

	@SuppressWarnings("unchecked")
	private void updateThoughts() {
		AppSettings settings = AppSettings.getInstance(this);
		String thoughtsJson = settings.getString(Key.SPEECH_BUBBLE_TEXT);
		JSONArray jarray = null;
		try {
			if (thoughtsJson != null && thoughtsJson.length() > 0)
				jarray = new JSONArray(thoughtsJson);
		} catch (JSONException je) {
			Log.e(TAG, je.getMessage(), je);
		}

		if (jarray != null) {
			thoughts = (ArrayList<String>) jarray.getMyArrayList();
		} else {
			thoughts = new ArrayList<String>();
		}

		changeQuote();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == 12) {
				updateThoughts();
			}
		}
	}

	public void drawQuote(final float[] fpx, final float[] fpy) {

		if (fpx == null) {
			changeQuote();
		}
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				mDrawSurface.setText(fpx, fpy, (quoteIndex == -1) ? "" : thoughts.get(quoteIndex));
			}
		});
		
	}

	private void changeQuote() {
		if (thoughts == null || thoughts.size() == 0) {
			quoteIndex = -1;
			return;
		}

		int index = 0 + (int) (Math.random() * ((thoughts.size() - 0) + 1));
		quoteIndex = index - 1;
		if (quoteIndex < 0)
			quoteIndex = 0;

	}

	private void overlay(Camera camera) {
		combined = new File(APP_FOLDER + "/IMG_" + FILE_NAME_FORMAT.format(new Date()) + ".jpg");
		Bitmap bmp1 = BitmapFactory.decodeFile(picture.getAbsolutePath());
		Bitmap bmp2 = BitmapFactory.decodeFile(speech.getAbsolutePath());
		//bmp2 = Bitmap.createScaledBitmap(bmp2, bmp1.getWidth(), bmp1.getHeight(), false);
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());

		Canvas canvas = new Canvas(bmOverlay);

		// Perform matrix rotations/mirrors depending on camera that took the photo
		//save the current config of canvas
		canvas.save();

		CameraInfo cameraInfo = new CameraInfo();
		Camera.getCameraInfo(cameraCurrentlyLocked, cameraInfo);
		if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			canvas.scale(-1.0f, 1.0f, bmp1.getWidth() * 0.5f, bmp1.getHeight() * 0.5f);
		}

		canvas.drawBitmap(bmp1, new Matrix(), null);
		//restore config to last save point
		canvas.restore();

		canvas.drawBitmap(bmp2, new Matrix(), null);
		writeToFile(combined, bmOverlay, false);

		bmp1.recycle();
		bmp2.recycle();
		bmOverlay.recycle();

		speech.delete();
		picture.delete();

		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(combined), "image/jpeg");
		startActivity(intent);

		//sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + APP_FOLDER)));
		MediaScannerConnection.scanFile(this,
		          new String[] { "file://" + APP_FOLDER }, null,
		          new MediaScannerConnection.OnScanCompletedListener() {
		      public void onScanCompleted(String path, Uri uri) {
		          Log.i("ExternalStorage", "Scanned " + path + ":");
		          Log.i("ExternalStorage", "-> uri=" + uri);
		      }
		 });
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			mCamera.stopPreview();
		
		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			//setContentView(R.layout.activity_main_land);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			//setContentView(R.layout.activity_main_port);
		}

		setCameraDisplayOrientation(cameraCurrentlyLocked, mCamera);
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			mCamera.startPreview();
	}

	public void setCameraDisplayOrientation(int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case android.view.Surface.ROTATION_0:
			degrees = 0;
			break;
		case android.view.Surface.ROTATION_90:
			degrees = 90;
			break;
		case android.view.Surface.ROTATION_180:
			degrees = 180;
			break;
		case android.view.Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		if(result == 180)
			result = 0;
		else if(result == 270)
			result = 90;
		
		if (result != 0 && info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			this.rotation = 360 - result;
		} else {
			this.rotation = result;
		}
		if(camera != null)
			camera.setDisplayOrientation(result);
	}
	
}
