package com.alimuzaffar.mindreader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class DrawSurfaceView extends View {

	private Paint mPaint;
	private Bitmap mBitmap;
	private String mText;
	private float[] fpx;
	private float[] fpy;

	public DrawSurfaceView(Context c, Paint paint) {
		super(c);
	}

	public DrawSurfaceView(Context context, AttributeSet set) {
		super(context, set);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		drawTextOnCanvas(canvas, null);
	}

	public void drawTextOnCanvas(Canvas canvas, String text) {
		// maybe color the bacground..
		//canvas.drawPaint(paint);
		// Setup a textview like you normally would with your activity context
		TextView tv = new TextView(getContext());

		if (fpx != null && fpy != null)
			tv.setVisibility(View.VISIBLE);
		else
			tv.setVisibility(View.GONE);

		// setup text
		tv.setText(this.mText);
		tv.setSingleLine(false);

		// maybe set textcolor
		tv.setTextColor(Color.BLACK);

		tv.setBackgroundResource(R.drawable.thought_bubble_up);

		// you have to enable setDrawingCacheEnabled, or the getDrawingCache will return null
		tv.setDrawingCacheEnabled(true);

		// we need to setup how big the view should be..which is exactly as big as the canvas
		tv.measure(MeasureSpec.makeMeasureSpec(canvas.getWidth(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(canvas.getHeight(), MeasureSpec.AT_MOST));
		// assign the layout values to the textview
		tv.layout(0, 0, tv.getMeasuredWidth(), tv.getMeasuredHeight());

		// draw the bitmap from the drawingcache to the canvas
		if (fpx != null && fpy != null) {
			float x = fpx[0] - (tv.getMeasuredWidth() - 100);
			float y = fpy[0] - (tv.getMeasuredHeight() + 100);
			
			if(x < 0) {
				x=0;
			}
			
			if(y < 0) {
				y = 0;
			}
			
			mBitmap = tv.getDrawingCache();
			canvas.drawBitmap(mBitmap, x, y, mPaint);
		} else {
			mBitmap = tv.getDrawingCache();
			canvas.drawBitmap(mBitmap, -1000, -1000, mPaint);
		}
		// disable drawing cache
		tv.setDrawingCacheEnabled(false);
	}

	public void setText(float[] fpx, float[] fpy, String text) {
		this.mText = text;
		this.fpx = fpx;
		this.fpy = fpy;
		invalidate();
	}
}
