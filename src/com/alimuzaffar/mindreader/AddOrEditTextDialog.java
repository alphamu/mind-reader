/*
 * (c) 2012 Vivant Digital Pty Ltd (Vivant) and its licensors
 *
 * Vivant grants Commonwealth Bank of Australia ACN 123 123 124 (Bank) an unrestricted royalty-free license to
 * use and commercially exploit (including by sub-licence) the mobile application and source code it
 * has developed as comprised in this software as set out in Order 10 between Vivant and the Bank.  For clarity,
 * the foregoing licence includes the right for the Bank to sub-license the mobile applications and/or
 * source code to any third parties at the Bank's discretion.
 */

package com.alimuzaffar.mindreader;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AddOrEditTextDialog extends DialogFragment implements View.OnClickListener {

	@SuppressWarnings("unused")
	private static final String TAG = AddOrEditTextDialog.class.getName();

	EditText text;
	int position = -1;

	public AddOrEditTextDialog() {
		super();
	}

	public static AddOrEditTextDialog newInstance() {
		AddOrEditTextDialog frag = new AddOrEditTextDialog();
		return frag;
	}

	public static AddOrEditTextDialog newInstance(int position, String text) {
		AddOrEditTextDialog frag = new AddOrEditTextDialog();
		Bundle bundle = new Bundle();
		bundle.putInt("position", position);
		bundle.putString("text", text);
		frag.setArguments(bundle);
		return frag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.speech_text, container, false);
		Button save = (Button) v.findViewById(R.id.save);
		save.setOnClickListener(this);
		Button cancel = (Button) v.findViewById(R.id.cancel);
		cancel.setOnClickListener(this);
		text = (EditText) v.findViewById(R.id.text);
		if (getArguments() != null) {
			position = getArguments().getInt("position", -1);
			text.setText(getArguments().getString("text"));
			getDialog().setTitle(R.string.add_quote);
		} else {
			getDialog().setTitle(R.string.edit_quote);
		}

		return v;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.save) {
			if (position == -1)
				((SpeechBubbleTextActivity) getActivity()).addText(text.getText().toString());
			else
				((SpeechBubbleTextActivity) getActivity()).updateText(position, text.getText().toString());
		}
		dismiss();

	}

}
