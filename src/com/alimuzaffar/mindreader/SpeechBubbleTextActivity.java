package com.alimuzaffar.mindreader;

import java.util.ArrayList;
import java.util.Stack;

import org.json.update.JSONArray;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.alimuzaffar.mindreader.AppSettings.Key;

public class SpeechBubbleTextActivity extends FragmentActivity implements View.OnClickListener {

	public static final String TAG = SpeechBubbleTextActivity.class.getSimpleName();
	
	private ArrayAdapter<String> arrayAdapter = null;
	private ArrayList<String> array = new ArrayList<String>();
	private ListView list = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.speech_bubble_text);

		AppSettings settings = AppSettings.getInstance(this);
		JSONArray jsonArray = null;
		String speecBubbleJson = settings.getString(Key.SPEECH_BUBBLE_TEXT);
		if (speecBubbleJson != null) {
			try {
				jsonArray = new JSONArray(speecBubbleJson);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			int length = jsonArray.length();
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					array.add(jsonArray.optString(i, ""));
				}
			}
		}
		list = (ListView) findViewById(R.id.list);
		arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, array);
		arrayAdapter.setNotifyOnChange(true);
		list.setAdapter(arrayAdapter);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		list.setEmptyView(findViewById(R.id.empty));

		Button add = (Button) findViewById(R.id.add);
		Button remove = (Button) findViewById(R.id.remove);

		add.setOnClickListener(this);
		remove.setOnClickListener(this);

		registerForContextMenu(list);

	}

	public void addText(String text) {
		array.add(text);
		
		save();
	}

	public void updateText(int position, String text) {
		array.remove(position);
		array.add(position, text);
		save();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add) {
			AddOrEditTextDialog dialog = AddOrEditTextDialog.newInstance();
			dialog.show(SpeechBubbleTextActivity.this.getSupportFragmentManager(), "add");
		} else if (v.getId() == R.id.remove) {
			SparseBooleanArray sparseBooleanArray = list.getCheckedItemPositions();
			if(sparseBooleanArray == null || sparseBooleanArray.size() == 0) {
				return;
			}
			GenericTwoButtonDialog dialog = GenericTwoButtonDialog.newInstance("Are you sure?", "This action cannot be undone.", R.string.no, R.string.yes);
			dialog.setOnClickPositiveButton(new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					int itemCount = list.getCount();

					SparseBooleanArray sparseBooleanArray = list.getCheckedItemPositions();
					Stack<Integer> checked = new Stack<Integer>();
					for (int i = 0; i < itemCount; i++) {
						if (sparseBooleanArray.get(i) == true) {
							checked.push(i);
							list.setItemChecked(i, false);
						}
					}
					//since we are using a stack
					//we'll remove the largest index item first.
					//that way when we remove an item and the list size
					//shrinks, we don't get an out of bounds exception
					//or remove the wrong item by mistake.
					while (checked != null && checked.size() > 0) {
						int i = checked.pop();
						Log.d(TAG, "remove item at "+i);
						array.remove(i);
					}
					save();
				}
			});

			dialog.show(getSupportFragmentManager(), "remove");

		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.item_long_click, menu);
		menu.setHeaderTitle("Options");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

		switch (item.getItemId()) {
		case R.id.edit:
			AddOrEditTextDialog dialog = AddOrEditTextDialog.newInstance(info.position, array.get(info.position));
			dialog.show(getSupportFragmentManager(), "edit");
			return true;
		case R.id.delete:
			GenericTwoButtonDialog dialog2 = GenericTwoButtonDialog.newInstance("Are you sure?", "This action cannot be undone.", R.string.no, R.string.yes);
			dialog2.setOnClickPositiveButton(new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					int i = info.position;
					array.remove(i);
					save();
				}
				
			});
			dialog2.show(getSupportFragmentManager(), "delete_one");
			
			return true;
		}
		return false;
	}
	
	private void save() {
		AppSettings.getInstance(SpeechBubbleTextActivity.this).set(Key.SPEECH_BUBBLE_TEXT, new JSONArray(array).toString());
		arrayAdapter.notifyDataSetChanged();
	}
	
	@Override
	public void onBackPressed() {
		Intent data = new Intent();
		if (getParent() == null) {
		    setResult(Activity.RESULT_OK, data);
		} else {
		    getParent().setResult(Activity.RESULT_OK, data);
		}
		finish();

	}

}
