/*
 * (c) 2012 Vivant Digital Pty Ltd (Vivant) and its licensors
 *
 * Vivant grants Commonwealth Bank of Australia ACN 123 123 124 (Bank) an unrestricted royalty-free license to
 * use and commercially exploit (including by sub-licence) the mobile application and source code it
 * has developed as comprised in this software as set out in Order 10 between Vivant and the Bank.  For clarity,
 * the foregoing licence includes the right for the Bank to sub-license the mobile applications and/or
 * source code to any third parties at the Bank's discretion.
 */

package com.alimuzaffar.mindreader;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;

public class GenericTwoButtonDialog extends DialogFragment {

	@SuppressWarnings("unused")
	private static final String TAG = GenericTwoButtonDialog.class.getName();

	private DialogInterface.OnClickListener done, cancel;
	private boolean useString = false;

	public GenericTwoButtonDialog() {
		super();
	}
	
	public static GenericTwoButtonDialog newInstance(Integer titleResId, Integer contentResId, Integer negativeBtnTxt, Integer positiveBtnTxt) {
		GenericTwoButtonDialog frag = new GenericTwoButtonDialog();
		Bundle args = new Bundle();
		args.putInt("title", titleResId);
		args.putInt("content", contentResId);
		args.putInt("cancel", (negativeBtnTxt == null) ? R.string.cancel : negativeBtnTxt.intValue());
		args.putInt("ok", (positiveBtnTxt == null) ? R.string.okay : positiveBtnTxt.intValue());
		frag.setArguments(args);
		return frag;
	}

	public static GenericTwoButtonDialog newInstance(String titleText, String contentText, Integer negativeBtnTxt, Integer positiveBtnTxt) {
		GenericTwoButtonDialog frag = new GenericTwoButtonDialog();
		Bundle args = new Bundle();
		args.putString("title", titleText);
		args.putString("content", contentText);
		args.putInt("cancel", (negativeBtnTxt == null) ? R.string.cancel : negativeBtnTxt.intValue());
		args.putInt("ok", (positiveBtnTxt == null) ? R.string.okay : positiveBtnTxt.intValue());
		frag.setArguments(args);
		frag.setUseString(true);
		return frag;
	}

	@TargetApi(11)
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle args = getArguments();
		
		AlertDialog.Builder builder;
		if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
			builder = new AlertDialog.Builder(getActivity());
		else
			builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
		
		builder.setNegativeButton(args.getInt("cancel"), (cancel != null) ? cancel : new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (getDialog() != null)
					getDialog().dismiss();

			}
		});
		
		if (done != null) {
			builder.setPositiveButton(args.getInt("ok"), done);
		}

		if (useString) {
			Spanned content = Html.fromHtml(args.getString("content"));
			if (args.getString("title") != null) {
				builder.setTitle(args.getString("title"));
				builder.setMessage(content);
			} else {
				//TextView tv = new TextView(getActivity());
				//tv.setText(content);
				//builder.setView(tv);
				builder.setMessage(content);
			}
		} else {
			if (args.getString("title") != null) {
				builder.setTitle(args.getInt("title"));
				builder.setMessage(args.getInt("content"));
			} else {
				//TextView tv = new TextView(getActivity());
				//tv.setText(args.getInt("content"));
				//builder.setView(tv);
				builder.setMessage(args.getInt("content"));
			}
		}

		return builder.create();

	}

	public void setOnClickPositiveButton(DialogInterface.OnClickListener onClick) {
		this.done = onClick;
	}

	public void setOnClickNegativeButton(DialogInterface.OnClickListener onClick) {
		this.cancel = onClick;
	}

	private void setUseString(boolean useString) {
		this.useString = useString;
	}
}
