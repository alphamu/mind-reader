package com.alimuzaffar.mindreader;

import org.json.update.JSONArray;

import com.alimuzaffar.mindreader.AppSettings.Key;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class BlankActivity extends Activity {

	Handler handler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blank_activity);

		AppSettings settings = AppSettings.getInstance(this);
		String text = settings.getString(Key.SPEECH_BUBBLE_TEXT);
		JSONArray jsonArray = null;
		try {
			if (text != null && text.length() > 0) {
				jsonArray = new JSONArray(text);
			} else {
				jsonArray = new JSONArray();
			}
			
			if(jsonArray.length() == 0) {
				jsonArray.put("I hope no one notices that I've soiled my pants.");
				jsonArray.put("Something smells rotten.");
				jsonArray.put("Have I left the stove on?");
				jsonArray.put("What they don't know is... I let the dogs out!");
				jsonArray.put("Mind Reader App is awesome! I should give it 5-stars!");
				settings.set(Key.SPEECH_BUBBLE_TEXT, jsonArray.toString());
			}
			
		} catch (Exception e) {	}
		
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("splash", true);
		startActivityForResult(intent, 1010101010);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			String cmd = data.getStringExtra("action");

			if (cmd.equals("close")) {
				finish();
			} else if (cmd.equals("restart")) {
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						Intent intent = new Intent(BlankActivity.this, MainActivity.class);
						intent.putExtra("splash", false);
						startActivityForResult(intent, 1010101010);

					}

				}, 200);

			} else {
				finish();
			}
		} else {
			finish();
		}
	}

}
