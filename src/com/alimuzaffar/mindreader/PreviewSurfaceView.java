package com.alimuzaffar.mindreader;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.alimuzaffar.mindreader.AppSettings.Key;

/**
 * A simple wrapper around a Camera and a SurfaceView that renders a centered
 * preview of the Camera
 * to the surface. We need to center the SurfaceView because not all devices
 * have cameras that
 * support preview sizes at the same aspect ratio as the device's display.
 */
public class PreviewSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
	private final String TAG = "Preview";

	SurfaceHolder mHolder;
	Size mPreviewSize, mPictureSize;
	List<Size> mSupportedPreviewSizes, mSupportedPictureSizes;
	Camera mCamera;

	public PreviewSurfaceView(Context context) {
		super(context);

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public PreviewSurfaceView(Context context, AttributeSet set) {
		super(context, set);

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void setCamera(Camera camera) {
		mCamera = camera;
		if (mCamera != null) {
			mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
			mSupportedPictureSizes = mCamera.getParameters().getSupportedPictureSizes();
			requestLayout();
		}
	}

	public void switchCamera(Camera camera) {
		setCamera(camera);
		try {
			camera.setPreviewDisplay(mHolder);
		} catch (IOException exception) {
			Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
		}
		Camera.Parameters parameters = camera.getParameters();
		parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		requestLayout();

		camera.setParameters(parameters);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, acquire the camera and tell it where
		// to draw.
		try {
			if (mCamera != null) {
				mCamera.setPreviewDisplay(holder);
			}
		} catch (IOException exception) {
			Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		if (mCamera != null) {
			mCamera.stopPreview();
		}
	}

	private Size getSetPreviewSize(List<Size> sizes, int w, int h) {
		AppSettings settings = AppSettings.getInstance(getContext());

		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		List<Camera.Size> csc = sizes;
		Camera.Size cs = null;
		if (settings.getBoolean(Key.FRONT_CAMERA)) {
			int index = settings.getInt(Key.FRONT_SELECTED_RESOLUTION, -1) % MainActivity.RES_ID;
			if (index > -1)
				cs = csc.get(index);

		} else {
			int index = settings.getInt(Key.SELECTED_RESOLUTION, -1) % MainActivity.RES_ID;
			if (index > -1)
				cs = csc.get(index);
		}

		if (cs != null)
			return cs;

		for (Camera.Size s : csc) {
			if (s.width == 640 && s.height == 480) {
				cs = s;
				break;
			}
		}
		if (cs == null)
			cs = csc.get(csc.size() / 2);

		return cs;
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		// Try to find an size match aspect ratio and size
		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		// Cannot find the one match the aspect ratio, ignore the requirement
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		if (mCamera == null) {
			return;
		}
		if (mSupportedPreviewSizes != null) {
			mPictureSize = getSetPreviewSize(mSupportedPictureSizes, w, h);
			mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, w, h);
		}

		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		Camera.Parameters parameters = mCamera.getParameters();
		
		parameters.setPictureSize(mPictureSize.width, mPictureSize.height);
		parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		requestLayout();

		mCamera.setParameters(parameters);
		mCamera.startPreview();
	}

	public void setScreenshot(PictureCallback rawPictureCallback, PictureCallback jpegPictureCallback) {
		mCamera.takePicture(null, rawPictureCallback, jpegPictureCallback);
	}

	public Camera getCamera() {
		return mCamera;
	}
	
	public Size getPreviewSize() {
		return mPreviewSize;
	}
	
	public Size getPictureSize() {
		return mPictureSize;
	}
	
	public void setPictureSize(Size size) {
		mPictureSize = size;
	}
}